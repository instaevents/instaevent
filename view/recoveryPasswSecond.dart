import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'busqueda.dart';
import 'busqueda_usr.dart';
import 'inicio.dart';
import 'iniciomain.dart';
import 'profileOne.dart';
import 'recoveryPassw.dart';
import 'recoveryPasswSecond.dart';


class RecoveryTwo extends StatelessWidget {
  const RecoveryTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Body(),
    );
  }
}

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //backgroundColor: Colors.black,
        body: Column(
          children: const [
            ContainerOne(),
            ContainerTwo(),
            ContainerThree(),
            ContainerTexto(),
            ContainerFour(),
            ContainerTextoTwo(),
            ContainerFive(),
            ContainerSix(),
          ],
        )
    );
  }
}

class ContainerOne extends StatelessWidget {
  const ContainerOne({super.key});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 50),
      height: 40,
      child: Text("Por favor, escriba el código que le llego a su correo.", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
    );
  }
}

class ContainerTwo extends StatelessWidget {
  const ContainerTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 45, right: 45),
        child: const CupertinoTextField()
    );
  }
}

class ContainerThree extends StatelessWidget {
  const ContainerThree({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(  top: 30),
      child: ElevatedButton(
        child: Text("\t\t\t\t ENVIAR \t\t\t\t"),
        onPressed: ()  {
          print("Esto funciona");
        },
      )
    );
  }
}

class ContainerTexto extends StatelessWidget {
  const ContainerTexto({super.key});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 90, right: 215),
      height: 20,
      child: Text("Nueva Contraseña", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
    );
  }
}

class ContainerFour extends StatelessWidget {
  const ContainerFour({super.key});
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 45, right: 45),
    child: const CupertinoTextField()
    );
  }
}

class ContainerTextoTwo extends StatelessWidget {
  const ContainerTextoTwo({super.key});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 40, right: 215),
      height: 20,
      child: Text("Repita Contraseña", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
    );
  }
}


class ContainerFive extends StatelessWidget {
  const ContainerFive({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 45, right: 45),
        child: const CupertinoTextField()
    );
  }
}


class ContainerSix extends StatelessWidget {
  const ContainerSix({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 150, top: 30),
        child: ElevatedButton(
          child: Text("\t\t\t\t RESETEAR \t\t\t\t"),
          onPressed: ()  {
            print("Esto funciona");
          },
        )
    );
  }
}