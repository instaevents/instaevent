import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'busqueda.dart';
import 'busqueda_usr.dart';
import 'inicio.dart';
import 'iniciomain.dart';
import 'profileOne.dart';
import 'recoveryPassw.dart';
import 'recoveryPasswSecond.dart';


class RecoveryOne extends StatelessWidget {
  const RecoveryOne({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Body(),
    );
  }
}

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
      //backgroundColor: Colors.black,
      body: Column(
        children: const [
          TextoTop(),
          TextoTopTwo(),
          Cupertino(),
          SizedBox(
            //Use of SizedBox
            height: 30,
          ),
          Boton(),
        ],
      )
    );
  }
}

class TextoTop extends StatelessWidget {
  const TextoTop({super.key});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top: 50),
      height: 40,
      child: Text("Datos de Recuperación", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
    );
  }
}

class TextoTopTwo extends StatelessWidget {
  const TextoTopTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text("\n\n\nPor favor, introduzca su email\n", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15), textAlign: TextAlign.center,);

  }
}

class Cupertino extends StatelessWidget {
  const Cupertino({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 45, right: 45),
        child: const CupertinoTextField()
    );
  }
}

class Boton extends StatelessWidget {
  const Boton({super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text("\t\t\t\t ENVIAR \t\t\t\t"),
      onPressed: ()  {
        print("Esto funciona");
      },
    );
  }
}

