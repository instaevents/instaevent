import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:instaevents/mvvm/logic/comprobarLogin.dart';
import 'package:instaevents/mvvm/view/recoveryPassw.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ComprobarCampos {
  ComprobarCampos();

  String? validaccionCampo(String strRetorno, String valor, int posicionCampo) {
    if (valor == null || identical(valor, "")) {
      return strRetorno;
    }
    // mejorar algoritmo
    switch (posicionCampo) {
      case 1:
        {
          if (!RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(valor)) return "Correo no valido";
        }
        break;
    }
    return null;
  }

}

class SendEmail {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  String email;

  SendEmail(
      this.email,
      );

  void resetPassword() async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      print("Correo enviado");
    } catch (e) {
      // Ocurrió un error al enviar el correo de recuperación de contraseña
      print('Error: $e');
    }
  }
}

