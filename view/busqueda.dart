import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'barraFooter.dart';
import 'busqueda_usr.dart';
import 'inicio.dart';



class Busqueda extends StatelessWidget{
  const Busqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Contenedor(),
        bottomNavigationBar: BarraNavegacionN(),
      ),
    );
  }
}


class Contenedor extends StatelessWidget{
  const Contenedor({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        BarraBusqueda(),
        OpcionesBusqueda()
      ],
    );
  }
}

class BarraBusqueda extends StatelessWidget{
  const BarraBusqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350, height: 50,
      child: CupertinoTextField(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blueGrey),
            borderRadius: BorderRadius.circular(10.0)
        ),
      ),
    );
    //--return const CupertinoTextField(placeholder: "Busca lo que imagines",);
  }
}

class BarraNavegacion extends StatelessWidget{
  const BarraNavegacion({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(
              top: BorderSide(color: Colors.grey)
          )
      ),
      child: Row(
        children: const [
          Expanded(child: Home()),
          Expanded(child: Search()),
          Expanded(child: Icon(Icons.person, color: Colors.black, size: 40)),
        ],
      ),
    );
  }
}

class OpcionesBusqueda extends StatelessWidget{
  const OpcionesBusqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 20),
        child: Row(
          children: const [
            Expanded(
                child: BuscarListaUsuario()
            ),
            Expanded(
                child: Icon(
                  Icons.map,
                  color: Colors.grey,
                  size: 45,
                )
            )
          ],
        )
    );
  }
}

class BuscarListaUsuario extends StatelessWidget{
  const BuscarListaUsuario({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ListaUsers())
        );
      },
      child: const Icon(
        Icons.person,
        color: Colors.grey,
        size: 45,
      ),
    );
  }

}


class Home extends StatelessWidget{
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Inicio())
        );
      },
      child: const Icon(
        Icons.home,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}

class Search extends StatelessWidget{
  const Search({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Busqueda())
        );
      },
      child: const Icon(
        Icons.search,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}
